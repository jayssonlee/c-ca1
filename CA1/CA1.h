#pragma once

#include <iostream>
#include <istream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <time.h>
#include <ctype.h>
#include <Windows.h>

using namespace std;
using std::cout;
using std::endl;
using std::string;
using std::cin;
using std::ifstream;
using std::ofstream;
using std::getline;
using std::to_string;
using std::sort;

class Movies
{
	private:
		int m_id;
		string m_name;
		string m_director;
		string m_genre;
		int m_release_year;

	public:
		Movies()
		{
			m_id = -1;
			m_name = "";
			m_director = "";
			m_genre = "";
			m_release_year = -1;
		}

		Movies(int id, string name, string director, string genre, int release_year)
		{
			m_id = id;
			m_name = name;
			m_director = director;
			m_genre = genre;
			m_release_year = release_year;
		}

		void setID(int id)
		{
			m_id = id;
		}

		int getID()
		{
			return m_id;
		}

		void setName(string name)
		{
			m_name = name;
		}

		string getName()
		{
			return m_name;
		}

		void setDirector(string director)
		{
			m_director = director;
		}

		string getDirector()
		{
			return m_director;
		}

		void setGenre(string genre)
		{
			m_genre = genre;
		}

		string getGenre()
		{
			return m_genre;
		}

		void setYear(int release_year)
		{
			m_release_year = release_year;
		}

		int getYear()
		{
			return m_release_year;
		}
};

class SortingComparator
{
	/* Comparator source: http://fusharblog.com/3-ways-to-define-comparison-functions-in-cpp/ */

	public:

		static bool idComparator(Movies &a, Movies &b)
		{
			return a.getID() < b.getID();
		}

		static bool nameComparator(Movies &a, Movies &b)
		{
			return a.getName() < b.getName();
		}

		static bool directorComparator(Movies &a, Movies &b)
		{
			return a.getDirector() < b.getDirector();
		}

		static bool genreComparator(Movies &a, Movies &b)
		{
			return a.getGenre() < b.getGenre();
		}

		static bool yearComparatorAscending(Movies &a, Movies &b)
		{
			return a.getYear() < b.getYear();
		}

		static bool yearComparatorDescending(Movies &a, Movies &b)
		{
			return a.getYear() > b.getYear();
		}
};

class HelperFunctions
{
	public:

		static bool isYearValid(int year)
		{
			// Getting current year: https://stackoverflow.com/questions/35258285/how-to-use-localtime-s-with-a-pointer-in-c

			struct tm newtime;
			time_t now = time(0);
			localtime_s(&newtime, &now);
			int currentYear = 1900 + newtime.tm_year;

			return ((year > 1950) && (year <= currentYear + 1));
			// + 1 enables user to add 'coming soon' movies for the following year
		}

		static bool isDigits(const std::string &str)
		{
			//source: https://stackoverflow.com/questions/19678572/how-to-validate-that-there-are-only-digits-in-a-string
			return str.find_first_not_of("0123456789") == std::string::npos;
		}

		static void printBreakLine()
		{
			cout << "\n---------------------------------------------------" << endl;
		}
};

int loadMovies(vector<Movies> &movie_data, int &lastMovieId);
int saveMovies(vector<Movies> movie_data);
int addMovie(vector<Movies> &movie_data, int &lastMovieId);
int deleteMovie(vector<Movies> &movie_data);
int editMovie(vector<Movies> &movie_data);
int searchMoviesByEntities(vector<Movies> movie_data);
int sortMovies(vector<Movies> movie_data);
int printComingSoonMovies(vector<Movies> movie_data);

void printMenu();
void printMovies(vector<Movies> movie_data);
void printMovie(Movies movie);