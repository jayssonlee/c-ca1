#include "stdafx.h"
#include "CA1.h"

int main()
{
	bool exit = false;

	int lastMovieId = 0;
	vector<Movies> movie_data;
	loadMovies(movie_data, lastMovieId);

	//source: http://www.cplusplus.com/forum/beginner/5830/
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	/*
		Colors used:
			Green for Inputs
			Red for Error Messages
			White for Data Output
			Purple for Selected Action
			Light Blue for Data Input
	*/

	printMenu();

	bool validInput = false;

	int action = -1;

	while(exit == false)
	{ 
		while (validInput == false)
		{
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10); //lightgreen
			cout << "\n Action -> ";

			cin >> action;

			cout << "" << endl;

			if (cin.good())
			{
				if (action >= 0 && action <= 8)
				{
					validInput = true;
				}
				else
				{
					cin.clear();
					cin.ignore(10000, '\n');
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
					cout << " Options only range from 1 to 7. Check input again" << endl;
					action = -1;
				}
			}
			else
			{
				cin.clear();
				cin.ignore(10000, '\n');
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
				cout << " Invalid input detected." << endl;
			}
		}

		validInput = false;

		switch (action)
		{
			case 0:
				printMenu();
				break;
			case 1:
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 13);
				cout << " Printing all movies" << endl;
				printMovies(movie_data);
				break;
			case 2:
				addMovie(movie_data, lastMovieId);
				break;
			case 3:
				editMovie(movie_data);
				break;
			case 4:
				deleteMovie(movie_data);
				break;
			case 5:
				searchMoviesByEntities(movie_data);
				break;
			case 6:
				sortMovies(movie_data);
				break;
			case 7:
				printComingSoonMovies(movie_data);
				break;
			case 8:
				saveMovies(movie_data);
				exit = true;
				break;
		}

		action = -1;
	}

	return 0;
}

void printMenu()
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	cout <<
		"------------------MENU-----------------------------" <<
		"\n    0 -> Print the menu options" <<
		"\n    1 -> List all movies" <<
		"\n    2 -> Add a movie" <<
		"\n    3 -> Edit a movie" <<
		"\n    4 -> Delete a movie" <<
		"\n    5 -> Search movies by Entities" << 
		"\n    6 -> Sort movies" <<
		"\n    7 -> List 'Coming Soon' Movies" <<
		"\n    8 -> Save and Exit" <<
		"\n---------------------------------------------------"
	<< endl; 
}

void printMovies(vector<Movies> movie_data)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	//vector<Movies>::size_type size = movie_data.size();
	for (unsigned int j = 0; j < movie_data.size(); j++)
	{
		if (j == 0)
		{
			HelperFunctions::printBreakLine();
		}
		cout << " ID:            " << movie_data[j].getID() <<
			"\n Name:          " << movie_data[j].getName() <<
			"\n Director:      " << movie_data[j].getDirector() <<
			"\n Genre:         " << movie_data[j].getGenre() <<
			"\n Release Year:  " << movie_data[j].getYear();
		HelperFunctions::printBreakLine();
	}
}

void printMovie(Movies movie)
{
	//output individual movie
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	HelperFunctions::printBreakLine();
	cout << " ID:            " << movie.getID() <<
		"\n Name:          " << movie.getName() <<
		"\n Director:      " << movie.getDirector() <<
		"\n Genre:         " << movie.getGenre() <<
		"\n Release Year:  " << movie.getYear();
	HelperFunctions::printBreakLine();
}

int addMovie(vector<Movies> &movie_data, int &lastMovieId)
{
	int newMovieId = lastMovieId + 1;
	string newMovieName = "";
	string newMovieDirector = "";
	string newMovieGenre = "";
	int newMovieReleaseYear = -1;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 13);
	cout << " Adding a movie" << endl;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	HelperFunctions::printBreakLine();

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 11);
	cout << " Name:          ";
	cin.ignore();
	getline(cin, newMovieName);

	cout << " Director:      ";
	getline(cin, newMovieDirector);

	cout << " Genre:         ";
	getline(cin, newMovieGenre);

	bool isValid = false;
	while (isValid == false)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 11);
		cout << " Release Year:  ";
		cin >> newMovieReleaseYear;

		if (cin.good() && HelperFunctions::isYearValid(newMovieReleaseYear))
		{
			isValid = true;
		}
		else
		{
			// https://stackoverflow.com/questions/5131647/why-would-we-call-cin-clear-and-cin-ignore-after-reading-input
			cin.clear();
			cin.ignore(10000, '\n');
			newMovieReleaseYear = -1;
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
			cout << "  --> Invalid year detected" << endl;
		}
	}

	Movies newMovie = Movies(newMovieId, newMovieName + " ", newMovieDirector + " ", newMovieGenre + " ", newMovieReleaseYear);

	bool exit = false;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
	cout << "\n Comfirm adding to database? --- 1 -> Yes / 2 -> No" << endl;
	int action = 0;

	while (exit == false)
	{
		cout << " -> ";
		cin >> action;

		if (cin.good())
		{
			if (action == 1)
			{
				movie_data.push_back(newMovie);
				exit = true;
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 11);
				cout << " Movie added";
			}
			else if (action == 2)
			{
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
				cout << " Cancelled";
				exit = true;
			}
			else
			{
				cin.clear();
				cin.ignore(10000, '\n');
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
				cout << " Only input of 1 or 2 is allowed." << endl;
				action = -1;
			}
		}
		else
		{
			cin.clear();
			cin.ignore(10000, '\n');
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
			cout << " Invalid input detected." << endl;
		}
	}

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	HelperFunctions::printBreakLine();

	return 0;
}

int deleteMovie(vector<Movies> &movie_data)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 13);
	cout << " Deleting a movie";

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	HelperFunctions::printBreakLine();

	bool exit = false;
	int id = -1;

	while (exit == false)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
		cout << "\n ID of the movie: ";
		cin >> id;

		if (!cin.good())
		{
			cin.clear();
			cin.ignore(10000, '\n');
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
			cout << " Wrong input detected" << endl;
			id = -1;
		}
		else
		{
			exit = true;
		}
	}

	bool idExist = false;

	for (unsigned int j = 0; j < movie_data.size(); j++)
	{
		if(movie_data[j].getID() == id)
		{
			movie_data.erase(movie_data.begin() + j);
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 11);
			cout << " Movie with ID " << j + 1 << " deleted";
			idExist = true;
			break;
		}
	}

	if (idExist == false)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
		cout << " No movie with this ID exist";
	}

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	HelperFunctions::printBreakLine();

	return 0;
}

int editMovie(vector<Movies> &movie_data)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 13);
	cout << " Editing a movie";

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	HelperFunctions::printBreakLine();

	int index = -1;

	int id = -1;
	bool exit = false;

	while (exit == false)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
		cout << "\n ID of the movie: ";
		cin >> id;

		if (!cin.good())
		{
			cin.clear();
			cin.ignore(10000, '\n');
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
			cout << " Wrong input detected" << endl;
			id = -1;
		}
		else
		{
			exit = true;
		}
	}

	bool idExist = false;

	for (unsigned int j = 0; j < movie_data.size(); j++)
	{
		if (movie_data[j].getID() == id)
		{
			index = j;
			idExist = true;
		}
	}

	if (idExist == false)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
		cout << "\n No movie with this ID exist";
	}
	else
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
		cout << "\n Original data:" << endl;

		printMovie(movie_data[index]);

		string newMovieName = "";
		string newMovieDirector = "";
		string newMovieGenre = "";
		int newMovieReleaseYear = -1;

		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
		cout << "\n New data:" << endl;

		cout << " Name:          ";
		cin.ignore();
		getline(cin, newMovieName);

		cout << " Director:      ";
		getline(cin, newMovieDirector);

		cout << " Genre:         ";
		getline(cin, newMovieGenre);

		bool isValid = false;
		while (isValid == false)
		{
			cout << " Release Year:  ";
			cin >> newMovieReleaseYear;

			if (cin.good() && HelperFunctions::isYearValid(newMovieReleaseYear))
			{
				isValid = true;
			}
			else
			{
				cin.clear();
				cin.ignore(10000, '\n');
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
				cout << "  --> Invalid year detected" << endl;
			}
		}

		bool exit = false;
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
		cout << "\n Comfirm to apply the edit? --- 1 -> Yes / 2 -> No" << endl;
		int action = 0;

		while (exit == false)
		{
			cout << " -> ";
			cin >> action;

			if (cin.good())
			{
				if (action == 1)
				{
					movie_data[index].setName(newMovieName + " ");
					movie_data[index].setDirector(newMovieDirector + " ");
					movie_data[index].setGenre(newMovieGenre + " ");
					movie_data[index].setYear(newMovieReleaseYear);
					exit = true;

					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 11);
					cout << " Movie editted";
				}
				else if (action == 2)
				{
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
					cout << " Cancelled";
					exit = true;
				}
				else
				{
					cin.clear();
					cin.ignore(10000, '\n');
					SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
					cout << " Wrong input detected." << endl;
					action = -1;
				}
			}
			else
			{
				cin.clear();
				cin.ignore(10000, '\n');
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
				cout << " Only input of 1 or 2 is allowed." << endl;
			}
		}
	}
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	HelperFunctions::printBreakLine();

	return 0;
}

int searchMoviesByEntities(vector<Movies> movie_data)
{
	//Search: https://stackoverflow.com/questions/2340281/check-if-a-string-contains-a-string-in-c

	vector<Movies> releventMoviesFound;

	bool movieFound = false;
	
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 13);
	cout << " Search a movie by ---> " << endl;
	cout << "" << endl;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	cout << "  Name     -> Insert 1" << endl;
	cout << "  Director -> Insert 2" << endl;
	cout << "  Genre    -> Insert 3" << endl;
	cout << "  Year     -> Insert 4" << endl;
	cout << "" << endl;

	bool validInput = false;
	int entity = -1;
	while (validInput == false)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
		cout << " Choice -> ";
		cin >> entity;

		if (cin.good())
		{
			if (entity > 0 && entity < 5)
			{
				validInput = true;
			}
			else
			{
				cin.clear();
				cin.ignore(10000, '\n');
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
				cout << " Options only range from 1 to 4. Check input again" << endl;
			}
		}
		else
		{
			cin.clear();
			cin.ignore(10000, '\n');
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
			cout << " Invalid input detected." << endl;
		}
	}

	if (entity != 4)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
		cout << "\n Keyword to search -> ";
	}
	else
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
		cout << "\n Search for year -> ";
	}
	
	string word = "";
	cin.ignore();
	getline(cin, word);

	for (unsigned int j = 0; j < movie_data.size(); j++)
	{
		switch (entity)
		{
			case 1:
				if (movie_data[j].getName().find(word) != std::string::npos)
				{
					releventMoviesFound.push_back(movie_data[j]);
					movieFound = true;
				}
				break;
			case 2:
				if (movie_data[j].getDirector().find(word) != std::string::npos)
				{
					releventMoviesFound.push_back(movie_data[j]);
					movieFound = true;
				}
				break;
			case 3:
				if (movie_data[j].getGenre().find(word) != std::string::npos)
				{
					releventMoviesFound.push_back(movie_data[j]);
					movieFound = true;
				}	
				break;
			case 4:
				if (HelperFunctions::isDigits(word))
				{
					if (movie_data[j].getYear() == stoi(word))
					{
						releventMoviesFound.push_back(movie_data[j]);
						movieFound = true;
					}
				}
				break;
		}
	}

	printMovies(releventMoviesFound);

	if (movieFound == false)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 11);
		cout << "\n No relevent movies found.";

		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
		HelperFunctions::printBreakLine();
	}
	return 0;
}

int sortMovies(vector<Movies> movie_data)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 13);
	cout << " Sort movies by ---> " << endl;
	cout << "" << endl;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	cout << "       Name         -> Insert 1" << endl;
	cout << "       Director     -> Insert 2" << endl;
	cout << "       Genre        -> Insert 3" << endl;
	cout << "  Year (Ascending)  -> Insert 4" << endl;
	cout << "  Year (Descending) -> Insert 5" << endl;
	cout << "" << endl;

	bool validInput = false;
	int entity = -1;
	while (validInput == false)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
		cout << " Choice -> ";
		cin >> entity;

		if (cin.good())
		{
			if (entity > 0 && entity < 6)
			{
				validInput = true;
			}
			else
			{
				cin.clear();
				cin.ignore(10000, '\n');
				SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
				cout << " Options only range from 1 to 5. Check input again" << endl;
			}
		}
		else
		{
			cin.clear();
			cin.ignore(10000, '\n');
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
			cout << " Invalid input detected." << endl;
		}
	}

	switch (entity)
	{
		case 1:
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 13);
			cout << "\n Sorting movies by Name" << endl;
			sort(movie_data.begin(), movie_data.end(), SortingComparator::nameComparator);
			printMovies(movie_data);
			break;
		case 2:
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 13);
			cout << "\n Sorting movies by Director" << endl;
			sort(movie_data.begin(), movie_data.end(), SortingComparator::directorComparator);
			printMovies(movie_data);
			break;
		case 3:
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 13);
			cout << "\n Sorting movies by Genre" << endl;
			sort(movie_data.begin(), movie_data.end(), SortingComparator::genreComparator);
			printMovies(movie_data);
			break;
		case 4:
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 13);
			cout << "\n Sorting movies by Year (Ascending)" << endl;
			sort(movie_data.begin(), movie_data.end(), SortingComparator::yearComparatorAscending);
			printMovies(movie_data);
			break;
		case 5:
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 13);
			cout << "\n Sorting movies by Year (Descending)" << endl;
			sort(movie_data.begin(), movie_data.end(), SortingComparator::yearComparatorDescending);
			printMovies(movie_data);
			break;
	}

	return 0;
}

int printComingSoonMovies(vector<Movies> movie_data)
{
	//print movies with next year release
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 13);
	cout << " Showing Coming Soon Movies:" << endl;

	vector<Movies> releventMoviesFound;

	bool movieFound = false;

	struct tm newtime;
	time_t now = time(0);
	localtime_s(&newtime, &now);
	int nextYear = 1900 + newtime.tm_year + 1;

	for (unsigned int j = 0; j < movie_data.size(); j++)
	{
		if (movie_data[j].getYear() == nextYear)
		{
			releventMoviesFound.push_back(movie_data[j]);
			movieFound = true;
		}
	}

	if (movieFound == false)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 11);
		cout << "\n Opps, No coming soon movies found.";

		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
		HelperFunctions::printBreakLine();
	}

	printMovies(releventMoviesFound);
	
	return 0;
}

int loadMovies(vector<Movies> &movie_data, int &lastMovieId)
{
	ifstream input("output.txt");

	string inputWord;

	int i = 0;

	if (!input)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
		cout << " Error reading the file / file doesn't exist" << endl;
		return -1;
	}

	do
	{
		bool validMovie = true;

		//---------------Read ID----------------------------------

		int movieId = 0;

		input >> movieId;

		if (movieId == 0)
		{
			break;
		}

		bool idExist = false;

		for (unsigned int j = 0; j < movie_data.size(); j++)
		{
			if (movie_data[j].getID() == movieId)
			{
				idExist = true;
				break;
			}
		}

		if (idExist == true)
		{
			validMovie = false;
		}

		if (lastMovieId < movieId)
		{
			lastMovieId = movieId;
		}

		//---------------READ NAME--------------------------------

		string movieName = "";

		do
		{
			input >> inputWord;
			movieName += inputWord;

			if (inputWord[inputWord.size() - 1] != ';')
			{
				movieName += " ";
			}
		} while (inputWord[inputWord.size() - 1] != ';');

		movieName[movieName.size() - 1] = ' ';


		//---------------READ DIRECTOR-----------------------------

		string movieDirector = "";

		do
		{
			input >> inputWord;
			movieDirector += inputWord;

			if (inputWord[inputWord.size() - 1] != ';')
			{
				movieDirector += " ";
			}
		} while (inputWord[inputWord.size() - 1] != ';');

		movieDirector[movieDirector.size() - 1] = ' ';

		//---------------READ GENRE--------------------------------
		
		string movieGenre = "";

		do
		{
			input >> inputWord;
			movieGenre += inputWord;

			if (inputWord[inputWord.size() - 1] != ';')
			{
				movieGenre += " ";
			}
		} while (inputWord[inputWord.size() - 1] != ';');

		movieGenre[movieGenre.size() - 1] = ' ';

		//----------------READ RELEASE DATE------------------------

		int movieReleaseYear = -1;

		input >> movieReleaseYear;

		//--------------------------------------------------------

		if (validMovie)
		{
			movie_data.push_back(Movies(movieId, movieName, movieDirector, movieGenre, movieReleaseYear));

			i++;
		}

	} while (!input.eof());

	input.close();

	sort(movie_data.begin(), movie_data.end(), SortingComparator::idComparator);

	return 0;
}

int saveMovies(vector<Movies> movie_data)
{
	ofstream output("output.txt");

	if (!output)
	{
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 12);
		cout << " Error writing the file" << endl;
		return -1;
	}

	for (unsigned int j = 0; j < movie_data.size(); j++)
	{
		int id = movie_data[j].getID();
		string name = movie_data[j].getName().substr(0, movie_data[j].getName().length() - 1);
		string director = movie_data[j].getDirector().substr(0, movie_data[j].getDirector().length() - 1);
		string genre = movie_data[j].getGenre().substr(0, movie_data[j].getGenre().length() - 1);
		int release_date = movie_data[j].getYear();

		output << id << " " << name << "; " << director << "; " << genre << "; " << release_date << endl;
	}

	output.close();

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	cout << " Saved successfully. Exitting...\n" << endl;

	return 0;
}